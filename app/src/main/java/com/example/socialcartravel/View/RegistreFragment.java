package com.example.socialcartravel.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.socialcartravel.ModelView.RegistreViewModel;
import com.example.socialcartravel.R;
import com.example.socialcartravel.Repository.Repository;
import com.example.socialcartravel.Repository.User;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;

public class RegistreFragment extends Fragment {

    private RegistreViewModel mViewModel;

    public static RegistreFragment newInstance() {
        return new RegistreFragment();
    }

    Button Regis;
    EditText nomP, Pcognom, Scognom, telefon, email, conemail, contrasenya, concontrasenya;
    private RegistreViewModel model;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registre_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegistreViewModel.class);
        // TODO: Use the ViewModel
        nomP.findViewById(R.id.nom);
        Pcognom.findViewById(R.id.Primer);
        Scognom.findViewById(R.id.Segon);
        telefon.findViewById(R.id.telefon);
        email.findViewById(R.id.email1);
        conemail.findViewById(R.id.email2);
        contrasenya.findViewById(R.id.contrasenya);
        concontrasenya.findViewById(R.id.contrasenya2);
        Regis.findViewById(R.id.Registrar);
    }
    public boolean CoincideixCorreu(){
        return email.getText().toString().equals(conemail.getText().toString());
    }
    public boolean CoincideixContrasenya(){
        return contrasenya.getText().toString().equals(concontrasenya.getText().toString());
    }

    public void Registrar2(View v)  {

        if (CoincideixCorreu()&&CoincideixContrasenya()) {

            String Nom = nomP.getText().toString();
            String lcognom = Pcognom.getText().toString();
            String llcognom = Scognom.getText().toString();
            String Telefon = telefon.getText().toString();
            String correu = email.getText().toString();
            String pass = contrasenya.getText().toString();

            Repository noR = new Repository();
            noR.createUser(Nom,lcognom, llcognom, correu, Telefon,pass);
            model = ViewModelProviders.of(this).get(RegistreViewModel.class);

            model.getResposta().observe(this, new Observer<JSONArray>() {
                @Override
                public void onChanged(JSONArray jsonArray) {
                    Log.d("TEST", "[onChanged]:"+ hashCode());
                }
            });
        }
        else
        {
            Snackbar dadesDiferents = Snackbar.make(v, "DADES INCORRECTES", Snackbar.LENGTH_LONG);
            dadesDiferents.show();
        }
    }

}
