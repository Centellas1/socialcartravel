package com.example.socialcartravel.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.socialcartravel.ModelView.CarViewModel;
import com.example.socialcartravel.ModelView.CercaViewModel;
import com.example.socialcartravel.ModelView.RegistreViewModel;
import com.example.socialcartravel.ModelView.SessioViewModel;
import com.example.socialcartravel.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Registrar(View v){
        Intent registrar = new Intent(this, RegistreViewModel.class);
        startActivity(registrar);
    }
    public void IniciarSessio(View v){
        Intent sessio = new Intent(this, CarViewModel.class);
        startActivity(sessio);
    }
    public void Cercar(View v){
        Intent cercar = new Intent(this, CercaViewModel.class);
        startActivity(cercar);
    }
}
