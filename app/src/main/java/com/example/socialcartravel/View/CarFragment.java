package com.example.socialcartravel.View;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.socialcartravel.ModelView.CarViewModel;
import com.example.socialcartravel.R;
import com.example.socialcartravel.Repository.Repository;

import org.json.JSONArray;

import java.util.List;

public class CarFragment extends Fragment {

    private CarViewModel mViewModel;


    public static CarFragment newInstance() {
        return new CarFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.car_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CarViewModel.class);
        // TODO: Use the ViewModel

        Repository noR = new Repository();
        noR.getCars(17);

        mViewModel.getResposta().observe(this.getViewLifecycleOwner(), new Observer<List<JSONArray>>() {
            @Override
            public void onChanged(List<JSONArray> jsonArray) {
                Log.d("TEST", "[onChanged]:" + hashCode());
            }
        });
    }

}
