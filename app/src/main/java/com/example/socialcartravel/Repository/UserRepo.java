package com.example.socialcartravel.Repository;

import com.example.socialcartravel.model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface UserRepo{
    @GET("users/{id_user}")
    Call<User> getUser(@Path("id_user") int id); //retorna usuari

    @POST("register")
    Call<String> createUser(@Field("nom") String Nom, @Field("1Cognom") String lCognom, @Field("2Cognom") String llCognom, @Field("email") String correu, @Field("telefon") String Telefon, @Field("contrasenya") String pass);

    @POST("login")
    Call<User> login(@Field("email") String correu, @Field("contrasenya") String pass);
}