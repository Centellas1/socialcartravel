package com.example.socialcartravel.Repository;

import com.example.socialcartravel.model.Car;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CarRepo{

   // @GET("cars")
   // Call <List<Car>> getCars(); //retornar els cotxe cotxes del usuari

    @GET("cars/{id_user}")
    Call <List<Car>> getCars(@Path("id_car") int user); //retornar els cotxe cotxes del usuari

    // @GET("cars/")
    // Call <List<Car>> getCar(@Path("id_car") int user); //retornar els cotxe cotxes del usuari

    @GET("cars/{id_car}")
    Call<Car> getCar(@Path("id_car") int car); //retornar el cotxe

    @POST("cars")
    Call<String> createCar(@Field("matricula") String Matricula, @Field("marca") String Marca, @Field("model") String Model, @Field("color") String Color, @Field("places") int Places, @Field("id_user") int User);

    @PUT("cars/{id_car}")
    Call<String> updateCar(@Path("id_car") int car,@Field("matricula") String Matricula, @Field("marca") String Marca, @Field("model") String Model, @Field("color") String Color, @Field("places") int Places, @Field("id_user") int User);


}

