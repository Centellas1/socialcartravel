package com.example.socialcartravel.Repository;

public class Car {
    private int id_user;
    private int id_car;
    private String matricula;
    private String marca;
    private String model;
    private int places;
    private String color;

    public void setCar( String Matricula, String Marca, String Model, String Color, int Places, int user ){
        id_user = user;
        matricula = Matricula;
        marca = Marca;
        model = Model;
        color = Color;
        places = Places;
    }


    public int getUser() {
        return id_user;
    }
    public int gerCar(){ return id_car;}
    public int getPlaces() {return places;}

    public String getMatricula(){
        return matricula;
    }
    public String getMarca(){
        return marca;
    }
    public String getModel(){
        return model;
    }
    public String getColor(){
        return color;
    }

}
