package com.example.socialcartravel.Repository;

import android.app.Application;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.socialcartravel.model.Car;
import com.example.socialcartravel.model.User;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {
    CarRepo carRepo;
    UserRepo userRepo;
    final String URL_BASE = "http://www.scttfg.site/v1";
    Application application;
    MutableLiveData<List<com.example.socialcartravel.model.Car>> cotxes = new MutableLiveData<>();
    MutableLiveData<Car> Car = new MutableLiveData<>();
    MutableLiveData<User> user = new MutableLiveData<>();
    MutableLiveData<String> usuari = new MutableLiveData<>();
    MutableLiveData<String> cotxe = new MutableLiveData<>();

    public Repository(Application application){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE).addConverterFactory(GsonConverterFactory.create())
                .build();
        userRepo = retrofit.create(UserRepo.class);
        carRepo = retrofit.create(CarRepo.class);
        this.application=application;
    }

    public LiveData<User> getPerfil(int id) {
        Call<User> call = userRepo.getUser(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                // https://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
                if(response.isSuccessful()){
                    user.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return user;
    }


    public LiveData<String> createUser(String Nom,String lcognom, String llcognom,String correu,String Telefon, String pass){

        Call<String> call = userRepo.createUser(Nom,lcognom, llcognom, correu, Telefon,pass);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //https://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
                if(response.isSuccessful()){
                    usuari.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return usuari;
    }

    public LiveData<User> login(String correu, String pass){
        Call<User> call = userRepo.login(correu, pass);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                // https://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
                if(response.isSuccessful()){
                    user.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return user;
    }

    // **************************************************************************************************************************
    // **************************************************************************************************************************
    // **************************************************        CARS       *****************************************************
    // **************************************************************************************************************************
    // **************************************************************************************************************************

    public LiveData<String> createCar(String Matricula,String Marca, String Model,String Color,int Places, int user){
        Call<String> call = carRepo.createCar(Matricula,Marca,Model,Color, Places, user);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //https://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
                if(response.isSuccessful()){
                    cotxe.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return cotxe;
    }

    public LiveData<Car> getCar(int car) {
        Call<Car> call = carRepo.getCar(car);

        call.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()){
                    Car.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return Car;
    }

    public LiveData<List<Car>> getCars(int user) {
        Call<List<Car>> call = carRepo.getCars(user);

        call.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                if(response.isSuccessful()){
                    cotxes.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return cotxes;
    }

    public LiveData<String> updateCar(int car,String Matricula,String Marca, String Model,String Color,int Places, int user){

        Call<String> call = carRepo.updateCar(car,Matricula,Marca,Model,Color, Places, user);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                   cotxe.postValue(response.body());
                }else{
                    Log.e("error", "Error inesperat");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("error", "Error inesperat");
            }
        });
        return cotxe;
    }

}
