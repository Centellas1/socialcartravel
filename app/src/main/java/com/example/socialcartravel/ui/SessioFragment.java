package com.example.socialcartravel.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.example.socialcartravel.ModelView.SessioViewModel;
import com.example.socialcartravel.R;
import com.example.socialcartravel.model.User;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.OnClick;

public class SessioFragment extends Fragment {

    private SessioViewModel mViewModel;
    EditText correu, pass;

    @BindView(R.id.IniciarSESSIO)
    Button Sessio;
    public static SessioFragment newInstance() {
        return new SessioFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sessio_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SessioViewModel.class);
        // TODO: Use the ViewModel
        correu.findViewById(R.id.correu);
        pass.findViewById(R.id.password);

    }

    @OnClick(R.id.IniciarSESSIO)
    public void onClickSessio(View v ) {
        if( correu != null && pass != null)
        {
            LiveData<User> user = mViewModel.login(correu.toString(), pass.toString());
            Snackbar dadesDiferents = Snackbar.make(v, "Hola," + user.getValue().getName(), Snackbar.LENGTH_LONG);
            dadesDiferents.show();

        }else
        {
            Snackbar dadesDiferents = Snackbar.make(v, "DADES INCORRECTES", Snackbar.LENGTH_LONG);
            dadesDiferents.show();
        }
    }

}