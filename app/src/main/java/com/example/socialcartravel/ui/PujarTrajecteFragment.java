package com.example.socialcartravel.ui;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.socialcartravel.ModelView.PujarTrajecteViewModel;
import com.example.socialcartravel.R;

import butterknife.BindView;
import butterknife.OnClick;

public class PujarTrajecteFragment extends Fragment {

    private PujarTrajecteViewModel mViewModel;

    EditText Hora, Dia, Desti, Origen, Preu, DiaF, HoraF;

    @BindView(R.id.PujarTrajecte)
    Button Crea;

    public static PujarTrajecteFragment newInstance() {
        return new PujarTrajecteFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pujar_trajecte_fragment, container, false);
        /*Hora.findViewById(R.id.HoraP);
        Dia.findViewById(R.id.DataP);
        Desti.findViewById(R.id.DestiP);
        Origen.findViewById(R.id.Porigen);
        Preu.findViewById(R.id.PreuP);
        DiaF.findViewById(R.id.dataPFinal);
        HoraF.findViewById(R.id.horaPFinal);*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PujarTrajecteViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick(R.id.PujarTrajecte)
    public void onPujarTrajecteCliked(View v){

    }


}