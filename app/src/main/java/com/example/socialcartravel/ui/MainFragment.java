package com.example.socialcartravel.ui;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.socialcartravel.ModelView.MainViewModel;
import com.example.socialcartravel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    @BindView(R.id.butoSessio)
    Button sessio;
    @BindView(R.id.butoRegistre)
    Button regis;
    @BindView(R.id.Cercar)
    Button cer;


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.butoSessio)
    public void onSESSIOClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_sessioFragment);
    }
    @OnClick(R.id.butoRegistre)
    public void onREGISTREClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_registreFragment);
    }
    @OnClick(R.id.Cercar)
    public void onCERCAClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_cercaFragment);
    }

}
