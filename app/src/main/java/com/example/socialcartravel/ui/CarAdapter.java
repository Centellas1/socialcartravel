package com.example.socialcartravel.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.socialcartravel.R;
import com.example.socialcartravel.model.Car;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarHolder> {
    List<Car> Cotxes = new ArrayList<>();

    public void setCotxes(List<Car> cotxes){
        this.Cotxes =cotxes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item,parent,false);
        return new CarHolder(view);
    }

    public void onBindViewHolder(@NonNull CarHolder holder, int position) {
        Car cotxes= Cotxes.get(position);
        holder.matricula.setText(cotxes.getMatricula());
    }

    @Override
    public int getItemCount() {
        return Cotxes.size();
    }

    public class CarHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.Matricula)
        TextView matricula;

        public CarHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }
}
