package com.example.socialcartravel.ui;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.socialcartravel.ModelView.Main2ViewModel;
import com.example.socialcartravel.R;
import com.google.android.material.tabs.TabItem;

import butterknife.BindView;
import butterknife.OnClick;

public class Main2Fragment extends Fragment {

    private Main2ViewModel mViewModel;

    TabItem avisos, historial, trajectes, inici, search, missatges, perfil;

    @BindView(R.id.PujarTrajecte)
    Button puja;

    public static Main2Fragment newInstance() {
        return new Main2Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main2_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(Main2ViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick(R.id.PujarTrajecte)
    public void onTrajecteCliked(View v)
    {
        Navigation.findNavController(v).navigate(R.id.PujarTrajecte);
    }

}
