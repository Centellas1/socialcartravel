package com.example.socialcartravel.ui;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.socialcartravel.ModelView.CercaViewModel;
import com.example.socialcartravel.R;

import butterknife.BindView;
import butterknife.OnClick;

public class CercaFragment extends Fragment {

    private CercaViewModel mViewModel;
    @BindView(R.id.CERCAR_Trajecte)
    Button Cercar;
    EditText origen, desti, hora, dia;

    public static CercaFragment newInstance() {
        return new CercaFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cerca_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CercaViewModel.class);
        // TODO: Use the ViewModel
        origen.findViewById(R.id.origen);
        desti.findViewById(R.id.desti);
        hora.findViewById(R.id.hora);
        dia.findViewById(R.id.dia);
    }

    @OnClick(R.id.CERCAR_Trajecte)
    public void onClickCerca(View v) {
        Navigation.findNavController(v).navigate(R.id.action_cercaFragment_to_carFragment);
    }

}
