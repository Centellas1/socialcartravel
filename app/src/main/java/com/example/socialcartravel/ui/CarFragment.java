package com.example.socialcartravel.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.socialcartravel.ModelView.CarViewModel;
import com.example.socialcartravel.R;
import com.example.socialcartravel.model.Car;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarFragment extends Fragment {

    @BindView(R.id.CarView2)
    RecyclerView CarView2;
    private CarViewModel mViewModel;
    CarAdapter adapter;

    public static CarFragment newInstance() {
        return new CarFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.car_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CarViewModel.class);
        // TODO: Use the ViewModel
        adapter = new CarAdapter();
        mViewModel.getResposta().observe(this.getViewLifecycleOwner(), this::onChangeCar);
        CarView2.setAdapter(adapter);
    }

    private void onChangeCar(List<Car> Cotxes) {
        adapter.setCotxes(Cotxes);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        CarView2.setHasFixedSize(true);
        CarView2.setLayoutManager(new LinearLayoutManager(this.getContext()));

    }

}
