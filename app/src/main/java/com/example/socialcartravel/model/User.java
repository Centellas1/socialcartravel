package com.example.socialcartravel.model;

public class User{
    //private static Retrofit retrofit =null;

    private int id_user;
    private String nom;
    private String Cognom1;
    private String Cognom2;
    private String email;
    private String telefon;
    private String pass;

    public void setUser( String Nom, String cognom1, String cognom2, String correu, String tel, String pas ){
        this.nom = Nom;
        this.Cognom1 = cognom1;
        this.Cognom2 = cognom2;
        this.email = correu;
        this.telefon = tel;
        this.pass = pas;
    }


    public int getUser() {
        return id_user;
    }

    public String getName(){
        return nom;
    }
    public String getCognom1(){
        return Cognom1;
    }
    public String getCognom2(){
        return Cognom2;
    }
    public String getEmail(){
        return email;
    }
    public String getTelefon(){
        return telefon;
    }
    public String getContra(){
        return pass;
    }
}
