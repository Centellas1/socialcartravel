package com.example.socialcartravel.ModelView;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.socialcartravel.model.Car;
import com.example.socialcartravel.Repository.Repository;

import java.util.List;

public class CarViewModel extends AndroidViewModel {
    // TODO: Implement the ViewModel
    Repository repository;

    public CarViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }

    public LiveData<List<Car>> getResposta() {
        return repository.getCars(17);
    }
}
