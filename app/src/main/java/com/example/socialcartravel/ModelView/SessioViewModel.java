package com.example.socialcartravel.ModelView;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.socialcartravel.Repository.Repository;
import com.example.socialcartravel.model.User;

public class SessioViewModel extends AndroidViewModel {
    Repository respository;
    public SessioViewModel(@NonNull Application application) {
        super(application);
        respository= new Repository(application);
    }
    // TODO: Implement the ViewModel
    public LiveData<User> login(String correu, String pass){
        return respository.login(correu,pass);
    }
}
