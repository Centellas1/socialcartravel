package com.example.socialcartravel.ModelView;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import com.example.socialcartravel.Repository.Repository;


public class RegistreViewModel extends AndroidViewModel {
    // TODO: Implement the ViewModel

    Repository repository;

    public RegistreViewModel(@NonNull Application application){
        super(application);
        repository=new Repository(application);
    }

    public LiveData<String> createUser(String Nom,String lcognom, String llcognom,String correu,String Telefon, String pass){
        return repository.createUser(Nom,lcognom,llcognom,correu,Telefon, pass);
    }


}
